#ifndef GAMEOBJECT_H__
#define GAMEOBJECT_H__

#include <list>
#include <string>
#include "Component.h"

class GameObject {
	std::list<Component*> components;
	std::string name;
public:
	GameObject();
	~GameObject();
	void Initialize(const std::string &name);
	void AddComponent(Component* component);
	void RemoveComponent(Component* component);
	void Update();
	void Display();
	std::string& GetName();
};

#endif