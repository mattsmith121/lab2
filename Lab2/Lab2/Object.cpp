#include "Object.h"
#include <iostream>
#include <string>

Object::Object() {
	initialized = false;
	name = "";
#if _DEBUG
	std::cout << "Object Created" << std::endl;
#endif
}

Object::~Object() {
#if _DEBUG
	std::cout << "Object Destroyed" << std::endl;
#endif
}

bool Object::IsInitialized() {
	return initialized;
}

std::string& Object::GetName() {
	return name;
}

void Object::Initialize(const std::string &name) {
	this->name = name;
	initialized = true;

#if _DEBUG
	std::cout << "Object Initialized" << std::endl;
#endif
}

void Object::Display() {
#if _DEBUG
	std::cout << "Object Display" << std::endl;
#endif
}