#include "AssetManager.h"
#include <iostream>

AssetManager::AssetManager() {
#if _DEBUG
	std::cout << "AssetManager Created" << std::endl;
#endif
}

AssetManager::~AssetManager() {
#if _DEBUG
	std::cout << "AssetManager Destroyed" << std::endl;
#endif
}

void AssetManager::Initialize() {
#if _DEBUG
	std::cout << "AssetManager Initialized" << std::endl;
#endif
}


void AssetManager::Display() {
#if _DEBUG
	std::cout << "AssetManager Display" << std::endl;
#endif
}

void AssetManager::Update() {
#if _DEBUG
	std::cout << "AssetManager Update" << std::endl;
#endif
}

