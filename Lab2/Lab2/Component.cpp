#include "Component.h"
#include <iostream>
#include <stdlib.h> 
#include <chrono>

Component::Component() {
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	id = rand(); // Random id between 0 and RAND_MAX
#if _DEBUG
	std::cout << "Component Created" << std::endl;
#endif
}

Component::~Component() {
#if _DEBUG
	std::cout << "Component Destroyed" << std::endl;
#endif
}

void Component::Initialize(){
#if _DEBUG
	std::cout << "Component Initialized" << std::endl;
#endif
}

void Component::Update() {
#if _DEBUG
	std::cout << "Component update" << std::endl;
#endif
}

void Component::Display() {
#if _DEBUG
	std::cout << "Component display" << std::endl;
#endif
}

int Component::GetComponentID() {
	return id;
}