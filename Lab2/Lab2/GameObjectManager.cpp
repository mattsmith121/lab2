#include "GameObjectManager.h"
#include <iostream>
#include <list>

GameObjectManager::GameObjectManager() {
#if _DEBUG
	std::cout << "GameObjectManager Created" << std::endl;
#endif
}

GameObjectManager::~GameObjectManager() {
	// Delete all the Game Objects
	while (gameObjects.size() > 0) {
		delete(gameObjects.back());
		gameObjects.pop_back();
	}
#if _DEBUG
	std::cout << "GameObjectManager Destroyed" << std::endl;
#endif
}

void GameObjectManager::Initialize(){
	gameObjects = std::list<GameObject*>();
#if _DEBUG
	std::cout << "GameObjectManager Initialized" << std::endl;
#endif
}

void GameObjectManager::AddGameObject(GameObject* gameObject) {
	gameObjects.push_back(gameObject);
}

void GameObjectManager::RemoveGameObject(GameObject* gameObject) {
	// Remove_if will call the destructor and reduce the size of the list
	gameObjects.remove_if([gameObject](GameObject* go) {
		return go->GetName() == gameObject->GetName();
	});
}

void GameObjectManager::Update() {
#if _DEBUG
	std::cout << "GameObjectManager Update" << std::endl;
#endif
	// Call update on GameObjects
	for (std::list<GameObject*>::iterator it = gameObjects.begin();
		it != gameObjects.end(); it++) {
		(*it)->Update();
	}
}

void GameObjectManager::Display() {
#if _DEBUG
	std::cout << "GameObjectManager Display" << std::endl;
#endif
	// Call display on GameObjects
	for (std::list<GameObject*>::iterator it = gameObjects.begin();
		it != gameObjects.end(); it++) {
		(*it)->Display();
	}
}