#ifndef ASSETMANAGER_H__
#define ASSETMANAGER_H__

class AssetManager {

public:
	AssetManager();
	~AssetManager();
	void Initialize();
	void Update();
	void Display();
};

#endif
