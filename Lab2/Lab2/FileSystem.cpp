#include "FileSystem.h"
#include <iostream>

FileSystem::FileSystem() {
#if _DEBUG
	std::cout << "FileSystem Created" << std::endl;
#endif
}

FileSystem::~FileSystem() {
#if _DEBUG
	std::cout << "FileSystem Destroyed" << std::endl;
#endif
}

void FileSystem::Initialize() {
#if _DEBUG
	std::cout << "FileSystem Initialized" << std::endl;
#endif
}

void FileSystem::Update() {
#if _DEBUG
	std::cout << "FileSystem Update" << std::endl;
#endif
}

void FileSystem::Display() {
#if _DEBUG
	std::cout << "FileSystem Display" << std::endl;
#endif
}