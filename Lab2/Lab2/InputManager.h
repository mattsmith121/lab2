#ifndef INPUTMANAGER_H__
#define INPUTMANAGER_H__

class InputManager {
public:
	InputManager();
	~InputManager();
	void Initialize();
	void Update();
	void Display();
};

#endif
