#ifndef FILESYSTEM_H__
#define FILESYSTEM_H__

class FileSystem {
public:
	FileSystem();
	~FileSystem();
	void Initialize();
	void Update();
	void Display();
};

#endif
