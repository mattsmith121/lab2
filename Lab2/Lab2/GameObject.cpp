#include "GameObject.h"
#include <iostream>
#include <list>

GameObject::GameObject() {
	name = "";
#if _DEBUG
	std::cout << "GameObject Created" << std::endl;
#endif
}

GameObject::~GameObject() {
	// Delete all the components
	while (components.size() > 0) {
		delete(components.back());
		components.pop_back();
	}
#if _DEBUG
	std::cout << "GameObject Destroyed" << std::endl;
#endif
}

void GameObject::Initialize(const std::string &name) {
	this->name = name;
	components = std::list<Component*>();
#if _DEBUG
	std::cout << "GameObject Initialized" << std::endl;
#endif
}

void GameObject::Display() {
#if _DEBUG
	std::cout << "GameObject Display" << std::endl;
#endif
	// Call display on components
	for (std::list<Component*>::iterator it = components.begin();
		it != components.end(); it++) {
		(*it)->Display();
	}
}

void GameObject::Update() {
#if _DEBUG
	std::cout << "GameObject Update" << std::endl;
#endif
	// Call update on components
	for (std::list<Component*>::iterator it = components.begin();
		it != components.end(); it++) {
		(*it)->Update();
	}
}

void GameObject::AddComponent(Component* component) {
	components.push_back(component);
}

void GameObject::RemoveComponent(Component* component) {
	// Remove_if will call the destructor and reduce the size of the list
	components.remove_if([component](Component* c) {
		return c->GetComponentID() == component->GetComponentID();
	});
}

std::string& GameObject::GetName(){
	return name;
}