#include "GameEngine.h"
#include "json.hpp"
#include <fstream>
#include <string>
#include <chrono>

GameEngine::GameEngine() {
	renderSystem = new RenderSystem();
	fileSystem = new FileSystem();
	inputManager = new InputManager();
	assetManager = new AssetManager();
	gameObjectManager = new GameObjectManager();
#if _DEBUG
	std::cout << "GameEngine Created" << std::endl;
#endif
}

GameEngine::~GameEngine() {
	// Delete all objects we created on heap
	delete(renderSystem);
	delete(fileSystem);
	delete(inputManager);
	delete(assetManager);
	delete(gameObjectManager);
#if _DEBUG
	std::cout << "GameEngine Destroyed" << std::endl;
#endif
}

void GameEngine::Initialize() {
	// Load in the GameSettings.json
	std::ifstream inputStreamSettings("GameSettings.json");
	std::string settingsString((std::istreambuf_iterator<char>(inputStreamSettings)),
		std::istreambuf_iterator<char>());

	json::JSON gameSettings = json::JSON::Load(settingsString);

	// Load in the default level (starting level)
	std::string defaultLevelString = gameSettings["GameEngine"]["DefaultFile"].ToString();

	// Now load in the level json
	std::ifstream inputStreamLevel(defaultLevelString);
	std::string levelString((std::istreambuf_iterator<char>(inputStreamLevel)),
		std::istreambuf_iterator<char>());

	json::JSON defaultLevel = json::JSON::Load(levelString);

	// Initialize other objects
	renderSystem->Initialize("Lab2", 400, 400, false);
	fileSystem->Initialize();
	inputManager->Initialize();
	assetManager->Initialize();
	gameObjectManager->Initialize();

#if _DEBUG
	std::cout << "GameEngine Initialized" << std::endl;
#endif
}

void GameEngine::GameLoop() {
#if _DEBUG
	std::cout << "GameEngine GameLoop" << std::endl;
#endif
	// Keep track of time
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed;
	// Run for ~10 seconds
	do {
		renderSystem->Update();
		fileSystem->Update();
		inputManager->Update(); 
		assetManager->Update();
		gameObjectManager->Update();
		
		// Update time
		elapsed = std::chrono::system_clock::now() - start;
	} while (elapsed.count() < 10.0);
}

void GameEngine::Display() {
#if _DEBUG
	std::cout << "GameEngine Display" << std::endl;
#endif
	// Keep track of time
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed;
	// Run for ~10 seconds
	do {
		renderSystem->Display();
		fileSystem->Display();
		inputManager->Display();
		assetManager->Display();
		gameObjectManager->Display();

		// Update time
		elapsed = std::chrono::system_clock::now() - start;
	} while (elapsed.count() < 10.0);
}