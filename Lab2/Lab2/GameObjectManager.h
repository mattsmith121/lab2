#ifndef GAMEOBJECTMANAGER_H__
#define GAMEOBJECTMANAGER_H__

#include <list>
#include "GameObject.h"

class GameObjectManager {
	std::list<GameObject*> gameObjects;
public:
	GameObjectManager();
	~GameObjectManager();
	void Initialize();
	void Update();
	void AddGameObject(GameObject* gameObject);
	void RemoveGameObject(GameObject* gameObject);
	void Display();
};

#endif
