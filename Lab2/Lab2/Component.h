#ifndef COMPONENT_H__
#define COMPONENT_H__

class Component {
	int id;
public:
	Component();
	~Component();
	void Initialize();
	int GetComponentID();
	void Update();
	void Display();
};

#endif