#ifndef RENDERSYSTEM_H__
#define RENDERSYSTEM_H__

#include <string>

class RenderSystem {
	std::string name;
	int width;
	int height;
	bool fullscreen;

public:
	RenderSystem();
	~RenderSystem();
	void Initialize(const std::string &name, int width, int height, bool fullscreen);
	void Update();
	void Display();
};

#endif