#ifndef OBJECT_H__
#define OBJECT_H__

#include <string>

class Object {
	bool initialized;
	std::string name;

public:
	Object();
	~Object();
	bool IsInitialized();
	std::string& GetName();
	void Initialize(const std::string &name);
	void Display();
};

#endif
