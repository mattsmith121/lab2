#ifndef GAMEENGINE_H__
#define GAMEENGINE_H__

#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"

class GameEngine {
	RenderSystem* renderSystem;
	FileSystem* fileSystem;
	InputManager* inputManager;
	AssetManager* assetManager;
	GameObjectManager* gameObjectManager;

public:
	GameEngine();
	~GameEngine();
	void Initialize();
	void GameLoop();
	void Display();
};

#endif