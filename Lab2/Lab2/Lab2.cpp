#include "GameEngine.h"

int main()
{
    // GameEngine safer on heap
    GameEngine* gameEngine = new GameEngine();
    // Initialize game engine
    gameEngine->Initialize();

    // Call Game Loop
    gameEngine->GameLoop();

    // Call display
    gameEngine->Display();

    // Free memory
    delete(gameEngine);
}