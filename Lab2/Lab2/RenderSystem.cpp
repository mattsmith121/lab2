#include "RenderSystem.h"
#include <iostream>

RenderSystem::RenderSystem() {
	name = "";
	width = 0;
	height = 0;
	fullscreen = false;
#if _DEBUG
	std::cout << "RenderSystem Created" << std::endl;
#endif
}

RenderSystem::~RenderSystem() {
#if _DEBUG
	std::cout << "RenderSystem Destroyed" << std::endl;
#endif
}

void RenderSystem::Initialize(const std::string &name, int width, int height, bool fullscreen) {
	this->name = name;
	this->width = width;
	this->height = height;
	this->fullscreen = fullscreen;
#if _DEBUG
	std::cout << "RenderSystem Initialized" << std::endl;
#endif
}

void RenderSystem::Update() {
#if _DEBUG
	std::cout << "RenderSystem Update" << std::endl;
#endif
}

void RenderSystem::Display() {
#if _DEBUG
	std::cout << "RenderSystem Display" << std::endl;
#endif
}