#include "InputManager.h"
#include <iostream>

InputManager::InputManager(){
#if _DEBUG
	std::cout << "InputManager Created" << std::endl;
#endif
}

InputManager::~InputManager(){
#if _DEBUG
	std::cout << "InputManager Destroyed" << std::endl;
#endif
}

void InputManager::Initialize() {
#if _DEBUG
	std::cout << "InputManager Initialized" << std::endl;
#endif
}

void InputManager::Update() {
#if _DEBUG
	std::cout << "InputManager Update" << std::endl;
#endif
}

void InputManager::Display() {
#if _DEBUG
	std::cout << "InputManager Display" << std::endl;
#endif
}

